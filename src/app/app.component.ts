import { Component} from '@angular/core';
import { trigger,animate,style,transition,keyframes } from '@angular/animations';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations:[
    trigger("moveInLeft",[
      transition("void=> *", [style({transform:"translateX(300px)"}),
      animate(200,keyframes([
        style({transform:"translateX(300px)"}),
        style({transform:"translateX(0"})
      ]))]),
      transition("*=>void",[style({transform:"translateX(0px)"}),
      animate(100,keyframes([
        style({transform:"translateX(0px)"}),
        style({transform:"translateX(300px)"})
      ]))])
    ])
  ]
})



export class AppComponent {
  todoArray=[]


  // click event
  addTodo(value){
    if(value != ''){

      this.todoArray.push(value)
    }
    else{
      alert('** Field Required **')
    }
    console.log(this.todoArray)
  }

  // delete item (but has issues if two have the same value.  Deletes multiple at a time)
  // deleteItem(todo){
  //   for(let i=0; i<= this.todoArray.length; i++){
  //     if(todo == this.todoArray[i]){
  //       this.todoArray.splice(i,1)
  //     }
  //   }
  //   console.log("delete item")
  // }

  // delete item (deletes the exact item clicked)
  deleteItem(index){
        this.todoArray.splice(index,1)
    console.log("delete item")
  }

  // submit form on enter
  todoSubmit(value:any){
    if(value.todo != ''){
      this.todoArray.push(value.todo)
    }else{
      alert('** Field Required **')
    }
    console.log(value)
  }

  title = 'todo-app';
}


